        <div class="footer row no-margin">
            <div class="col-xs-4 nopad text-left">
                &copy; <?php echo date('Y') ?> Pitney Bowes, Inc
            </div>

            <div class="col-xs-4 nopad text-center">
                <a href="http://www.pitneybowes.com/us/customer-engagement-marketing/synchronized-communications-execution/engageone-video-personalized-video.html" target="_blank" tabindex="5">
                    <span class="translate" data-translate="PoweredByFooterText">Powered by</span> Pitney Bowes <br>
                    <strong>EngageOne&reg; Video</strong>
                </a>
            </div>

            <div class="col-xs-4 nopad text-right">
                <a href="#" class="translate" data-translate="HelpFooterText" target="_blank" tabindex="5">Help</a> |
                <a href="#" class="translate" data-translate="PrivacyPolicyFooterText" target="_blank" tabindex="5">Privacy Policy</a>
            </div>
        </div>