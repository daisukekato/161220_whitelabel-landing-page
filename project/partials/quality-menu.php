<div id="jsQualitySelectorPopout" class="timeline-control-quality__popout">

    <div id="jsQualityMenuTitle" class="quality-menu-title menu-title hover quality" tabindex="2">
        <div class="float-left">
            <div id="jsQualityLeftArrow" class="setting-arrow left"></div>
        </div>
        <div id="jsQualityMenuTitleText" class=quality-menu-text setting-text">
            <span class="translate" data-translate="QualityMenuText">Quality</span>
            <span class="setting-light-text js-quality-text"> (Auto)</span>
        </div>
        <div class="clear-both"></div>
    </div>

    <div class="timeline-control-quality__item" data-quality="auto" tabindex="2">
        <div class="quality-auto-tick-container">
            <span id="jsautoTickIcon" class="selected-icon">&#10004;</span>
        </div>
        <div class="quality-item-text">Auto</div>
        <div class="clear-both"></div>
    </div>
    <div class="timeline-control-quality__item" data-quality="1080p" tabindex="2">
        <div class="quality-auto-tick-container">
            <span id="js1080pAutoIcon" class="auto-icon">&#8226;</span>
            <span id="js1080pTickIcon" class="selected-icon">&#10004;</span>
        </div>
        <div class="quality-item-text">1080p</div>
        <div class="quality-hd-icon-container">
            <div class="hd-icon"></div>
        </div>
        <div class="clear-both"></div>
    </div>
    <div class="timeline-control-quality__item" data-quality="720p" tabindex="2">
        <div class="quality-auto-tick-container">
            <span id="js720pAutoIcon" class="auto-icon">&#8226;</span>
            <span id="js720pTickIcon" class="selected-icon">&#10004;</span>
        </div>
        <div class="quality-item-text">720p</div>
        <div class="quality-hd-icon-container">
            <div class="hd-icon"></div>
        </div>
        <div class="clear-both"></div>
    </div>
    <div class="timeline-control-quality__item" data-quality="540p" tabindex="2">
        <div class="quality-auto-tick-container">
            <span id="js540pAutoIcon" class="auto-icon">&#8226;</span>
            <span id="js540pTickIcon" class="selected-icon">&#10004;</span>
        </div>
        <div class="quality-item-text">540p</div>
        <div class="clear-both"></div>
    </div>
    <div class="timeline-control-quality__item" data-quality="360p" tabindex="2">
        <div class="quality-auto-tick-container">
            <span id="js360pAutoIcon" class="auto-icon">&#8226;</span>
            <span id="js360pTickIcon" class="selected-icon">&#10004;</span>
        </div>
        <div class="quality-item-text">360p</div>
        <div class="clear-both"></div>
    </div>
    
</div>